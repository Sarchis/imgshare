const ctrl = {}


ctrl.index = (req, res) => {
    res.status(200).json({
        ok: true,
        mensaje: 'Página de inicio'
    })
}

module.exports = ctrl;
